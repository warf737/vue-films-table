import Vue from "vue";
import App from "./App.vue";
import Router from "vue-router";
import Film from './components/Film.vue';
import FavoriteList from './components/FavoriteList.vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Stub from './components/Stub'
import Main from './components/Main';
Vue.use(Router);
Vue.use(ElementUI);

const router = new Router({
  routes: [
      {
          path:'/film/main',
          name: 'main',
          component: Main
      },
      {
          path: '/film/stub',
          name:'stub',
          component: Stub,
          props: true,
      },
      {
          path: '/film/:id',
          name:'film',
          component: Film,
          props: true,
    },{
          path: '/film/favorites',
          name:'favorites',
          component: FavoriteList,
          props: true,
    }
  ],
    mode: 'history'
});
router.replace('/film/stub');
new Vue({
  render: h => h(App),
  router
}).$mount("#app");
